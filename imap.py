from imaplib import IMAP4_SSL
import email
import hashlib
import time
import random
import os

# Config
baseDir = "./srcComments/"
domain = "imap.gmail.com"
with open(".creds", "r") as c:
    user, pswd = c.read().strip().split("\n")

# Extract body from email
def get_first_text_block(email_message_instance):
    maintype = email_message_instance.get_content_maintype()
    if maintype == 'multipart':
        for part in email_message_instance.get_payload():
            if part.get_content_maintype() == 'text':
                return part.get_payload(decode=True)
    elif maintype == 'text':
        return email_message_instance.get_payload()

def decodeHeader(raw):
    dh = email.header.decode_header(raw)
    return "".join([str(t[0], t[1] or "UTF-8") if isinstance(t[0], bytes)
        else t[0] for t in dh])

# Main
mBox = IMAP4_SSL(domain)
mBox.login(user, pswd)
mBox.select("inbox")
# Get list of mail
res, data = mBox.uid("search", None, "ALL")
uids = data[0].split()

for uid in uids:
    try:
        res, data = mBox.uid("fetch", uid, "(RFC822)")
        message = email.message_from_string(data[0][1].decode())

        body = get_first_text_block(message).decode()

        bodySplit = body.split("\n", 1)
        firstLine = bodySplit[0].split("=", 1)
        if firstLine[0] == "name":
            author = firstLine[1]
            body = bodySplit[1]
        else:
            authorRaw = email.utils.parseaddr(message["From"])[0]
            author = decodeHeader(authorRaw)
        authorHash = hashlib.md5(author.encode()).hexdigest()[:6]

        body = body.replace("\n", "\n  ").replace("\r", "")

        date = email.utils.parsedate(message["Date"])
        dateStr = time.strftime("%Y-%m-%d %H:%M:%S", date)

        subject = decodeHeader(message["Subject"]).strip()
        subjectSplit = message["Subject"].rsplit("/", 1)
        title = subjectSplit[1] if len(subjectSplit) > 1 else ""
        rand = str(random.randint(1, 1000000))
        if not os.path.exists(baseDir + title):
            os.makedirs(baseDir + title)
        filename = baseDir + title + "/" + dateStr + + "-" + authorHash + rand

        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with open(filename, "w") as out:
            out.write("---\n")
            out.write("post_id: " + subject + "\n")
            out.write("name: " + author + "\n")
            out.write("date: " + dateStr + " UTC\n")
            out.write("comment: |\n" + body)
    except Exception:
        print("[Error] at " + uid.decode())

mBox.logout()

