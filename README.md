JekyllAutoComment
===
A Python 3 script that automatically fetches mail from an IMAP server, creating comments for [jekyll-static-comments](https://github.com/mpalmer/jekyll-static-comments).

Note that since the jekyll-static-comments plugin does not defend against XSS, it is recommended to use this with [JekyllSanitize](https://gitlab.com/willypillow/JekyllSanitize).

Usage
---
* Change the `baseDir` variable in `imap.py` to the directory that you want to place your comments in.
* Change the `domain` variable in `imap.py` to your IMAP server domain.
* Create a file `.creds` with your IMAP username as the first line and the password as the second line.
* Run `python3 imap.py`.

Author Name
---
If the first line of the email is in the format `name=$name`, then `$name` will be set as the name of the author of the comment. Otherwise, the name of the email sender is used.
